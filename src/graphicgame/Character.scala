

package graphicgame

abstract class Character(var x: Int, var y: Int) {
  //var level: Level
  var dead = false

  def move(dX: Int, dY: Int): Unit = {
    x = dX
    y = dY
  }
  

  def moveTorward(dX: Int, dY: Int): Unit = {

  }
  
  def die(): Unit
  def setLevel(level: Level): Unit
  def update(): Unit
}