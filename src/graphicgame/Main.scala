
package graphicgame

import scala.io.Source
import scalafx.Includes.eventClosureWrapperWithParam
import scalafx.Includes.jfxKeyEvent2sfx
import scalafx.animation.AnimationTimer
import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.canvas.Canvas
import scalafx.scene.input.KeyCode
import scalafx.scene.input.KeyEvent
import scala.collection.mutable._
import scalafx.scene.paint.Color
import scalafx.scene.text.Font

object Main {
  def main(args: Array[String]): Unit = {
    val app = new JFXApp {
      stage = new JFXApp.PrimaryStage {
        title = "Graphical Game"
        scene = new Scene(1000, 1000) {
          val canvas = new Canvas(1000, 1000)
          val gc = canvas.graphicsContext2D

          content = List(canvas)

          val render = new Render
          var levelList: ListBuffer[Level] = ListBuffer[Level]()
          val mapSource = Source.fromFile("map.txt")
          val mapLines = mapSource.getLines
          val numLevels = mapLines.next.toInt

          for (i <- 0 until numLevels) {
            val row = mapLines.next.toInt
            val col = mapLines.next.toInt
            val numEnemies = mapLines.next.toInt

            var enemyList = ListBuffer[Character]()
            for (i <- 0 until numEnemies) {
              enemyList += new Enemy(mapLines.next.toInt, mapLines.next.toInt)
            }

            val stairsDestX = mapLines.next.toInt
            val stairsDestY = mapLines.next.toInt

            val mapArray = Array.ofDim[MapElements](col, row)
            for (i <- 0 until row) {
              val nextLine = mapLines.next()
              for (j <- 0 until col) {
                val nextVal = nextLine(j).asDigit
                nextVal match {
                  case 0 => mapArray(j)(i) = Floor
                  case 1 => mapArray(j)(i) = Wall
                  case 2 => mapArray(j)(i) = new Stairs(stairsDestX, stairsDestY)
                  case _ => mapArray(j)(i) = Blank
                }
              }
            }
            levelList += new Level(0, mapArray, enemyList)
          }

          //GameEngine?
          for (i <- 0 until levelList.length) {
            for (j <- levelList(i).elem) {
              for (k <- j) {
                if (k.isInstanceOf[Stairs] && i != levelList.length - 1) k.asInstanceOf[Stairs].setDestLevel(levelList(i + 1))
              }
            }
          }

          for (i <- levelList) {
            for (j <- i.characters) {
              j.setLevel(i)
            }
          }

          val pc = new Player(1, 1, 3)
          levelList(0).addCharacter(pc)

          onKeyPressed = (e: KeyEvent) => {
            e.code match {
              case KeyCode.A => pc.leftPressed
              case KeyCode.D => pc.rightPressed
              case KeyCode.W => pc.upPressed
              case KeyCode.S => pc.downPressed
              case _ =>
            }
          }

          onKeyReleased = (e: KeyEvent) => {
            e.code match {
              case KeyCode.Left => pc.shoot(3)
              case KeyCode.Right => pc.shoot(1)
              case KeyCode.Up => pc.shoot(0)
              case KeyCode.Down => pc.shoot(2)
              case _ =>
            }
          }

          var gameLevel = pc.pLevel

          var lastTime = 0L
          val timer: AnimationTimer = AnimationTimer(t => {
            val delay = (t - lastTime) / 1e9
            if (lastTime > 0 && delay > .1) {
              if (gameLevel.characters.contains(pc)) {
                gameLevel = pc.pLevel
              }

              gameLevel.characters.foreach(c => c match {
                case e: Enemy =>
                  if (!e.dead) {
                    e.moveCount += 1
                    if (e.moveCount % 2 == 0) {
                      val path = e.moveTowards(c.x, c.y, pc.x, pc.y)
                      if (path != null) e.move(path._1, path._2)
                    }
                  }
                case p: Projectile =>
                  p.move()
                case _ =>
              })
              render.render(gc, gameLevel.elem.length, gameLevel.elem(0).length, gameLevel)
              gc.fill = Color.Red
              gc.font = new Font(20)
              gc.fillText("Score: " + pc.score, 10, 20)
              lastTime = t - ((delay - 0.1) * 1000000000).toInt
            }
            if (lastTime == 0) lastTime = t
          })
          timer.start()
        }
      }
    }
    app.main(args)
  }
}