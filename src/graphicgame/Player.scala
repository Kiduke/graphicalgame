

package graphicgame

class Player(pX: Int, pY: Int, pDir: Int) extends Character(pX, pY) {
  var dir = pDir
  var pLevel: Level = null
  var score = 0
  
  def move(nX: Int, nY: Int, level: Level): Unit = {
    x = nX
    y = nY
    pLevel = level
    level.addCharacter(this)
  }

  def update(): Unit = {

  }

  def setLevel(level: Level) = {
    pLevel = level
  }

  def upPressed(): Unit = {
    if (dead) {}
    else if (pLevel.element(x, y - 1).canPass(this) && !pLevel.elemOccupied(x, y - 1)) {
      dir = 0
      move(x, y - 1)
    } else if (pLevel.elemOccupied(x, y - 1) && pLevel.getElemChara(x, y - 1).isInstanceOf[Enemy]) die()
    else if (pLevel.element(x, y - 1).isInstanceOf[Stairs]) {
      move(pLevel.element(x, y - 1).asInstanceOf[Stairs].destX, pLevel.element(x, y - 1).asInstanceOf[Stairs].destY, pLevel.element(x, y - 1).asInstanceOf[Stairs].destLevel)
    }
  }

  def downPressed(): Unit = {
    if (dead) {}
    else if (pLevel.element(x, y + 1).canPass(this) && !pLevel.elemOccupied(x, y + 1)) {
      dir = 2
      move(x, y + 1)
    } else if (pLevel.elemOccupied(x, y + 1) && pLevel.getElemChara(x, y + 1).isInstanceOf[Enemy]) die()
    else if (pLevel.element(x, y + 1).isInstanceOf[Stairs]) {
      move(pLevel.element(x, y + 1).asInstanceOf[Stairs].destX, pLevel.element(x, y + 1).asInstanceOf[Stairs].destY, pLevel.element(x, y + 1).asInstanceOf[Stairs].destLevel)
    }
  }

  def rightPressed(): Unit = {
    if (dead) {}
    else if (pLevel.element(x + 1, y).canPass(this) && !pLevel.elemOccupied(x + 1, y)) {
      dir = 1
      move(x + 1, y)
    } else if (pLevel.elemOccupied(x + 1, y) && pLevel.getElemChara(x + 1, y).isInstanceOf[Enemy]) die()
    else if (pLevel.element(x + 1, y).isInstanceOf[Stairs]) {
      move(pLevel.element(x + 1, y).asInstanceOf[Stairs].destX, pLevel.element(x + 1, y).asInstanceOf[Stairs].destY, pLevel.element(x + 1, y).asInstanceOf[Stairs].destLevel)
    }
  }

  def leftPressed(): Unit = {
    if (dead) {}
    else if (pLevel.element(x - 1, y).canPass(this) && !pLevel.elemOccupied(x - 1, y)) {
      dir = 3
      move(x - 1, y)
    } else if (pLevel.elemOccupied(x - 1, y) && pLevel.getElemChara(x - 1, y).isInstanceOf[Enemy]) die()
    else if (pLevel.element(x - 1, y).isInstanceOf[Stairs]) {
      move(pLevel.element(x - 1, y).asInstanceOf[Stairs].destX, pLevel.element(x - 1, y).asInstanceOf[Stairs].destY, pLevel.element(x - 1, y).asInstanceOf[Stairs].destLevel)
    }
  }

  def shoot(nDir: Int): Unit = {
    if (dead) {}
    else {
      val bullet = new Projectile(x, y, nDir, pLevel, this)
      pLevel.addCharacter(bullet)
    }
  }

  def die() {
    pLevel.removeCharacter(this)
    dead = true
  }
}