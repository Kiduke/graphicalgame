package graphicgame

class Projectile(pX: Int, pY: Int, pDir: Int, pLevel: Level, val master: Player) extends Character(pX, pY) {
  var dir = pDir
  def move() = {
    dir match {
      case 0 =>
        if (pLevel.element(x, y - 1).canPass(this) && !pLevel.elemOccupied(x, y - 1)) {
          y -= 1
        } else if (pLevel.elemOccupied(x, y - 1) && pLevel.getElemChara(x, y - 1).isInstanceOf[Enemy]) {
          pLevel.getElemChara(x, y - 1).die()
          master.score += 1
          die()
        } else die()
      case 1 =>
        if (pLevel.element(x + 1, y).canPass(this) && !pLevel.elemOccupied(x + 1, y)) {
          x += 1
        } else if (pLevel.elemOccupied(x + 1, y) && pLevel.getElemChara(x + 1, y).isInstanceOf[Enemy]) {
          pLevel.getElemChara(x + 1, y).die()
          master.score += 1
          die()
        } else die()
      case 2 =>
        if (pLevel.element(x, y + 1).canPass(this) && !pLevel.elemOccupied(x, y + 1)) {
          y += 1
        } else if (pLevel.elemOccupied(x, y + 1) && pLevel.getElemChara(x, y + 1).isInstanceOf[Enemy]) {
          pLevel.getElemChara(x, y + 1).die()
          master.score += 1
          die()
        } else die()
      case 3 =>
        if (pLevel.element(x - 1, y).canPass(this) && !pLevel.elemOccupied(x - 1, y)) {
          x -= 1
        } else if (pLevel.elemOccupied(x - 1, y) && pLevel.getElemChara(x - 1, y).isInstanceOf[Enemy]) {
          pLevel.getElemChara(x - 1, y).die()
          master.score += 1
          die()
        } else die()
    }
  }
  def setLevel(level: Level) = {}
  def die() {
    pLevel.removeCharacter(this)
  }

  def update(): Unit = {}
}