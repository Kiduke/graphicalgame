

package graphicgame

import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.canvas.Canvas
import scalafx.scene.paint.Color
import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.image.Image

class Render {
  val playerImg = new Image("file:src/images/ufo.png", true)
  val bulletImg = new Image("file:src/images/bullet.png", true)
  val enemyImg = new Image("file:src/images/enemy.png", true)
  val wallImg = new Image("file:src/images/wall.jpg", true)
  val floorImg = new Image("file:src/images/space.jpg", true)
  val stairsImg = new Image("file:src/images/stairs.png", true)

  def render(g: GraphicsContext, cx: Int, cy: Int, level: Level): Unit = {

    g.fill = Color.Blue
    g.fillRect(0, 0, 1000, 1000)
    val cellWidth = 1000 / cx
    val cellHeight = 1000 / cy

    for (i <- 0 until cx; j <- 0 until cy) {
      if (level.element(i, j).isInstanceOf[Stairs]) {
        g.drawImage(stairsImg, i * cellWidth, j * cellHeight, cellWidth, cellHeight)
      }
      level.element(i, j) match {
        case Floor =>
          g.drawImage(floorImg, i * cellWidth, j * cellHeight, cellWidth, cellHeight)
        case Wall =>
          g.drawImage(wallImg, i * cellWidth, j * cellHeight, cellWidth, cellHeight)
        case Blank =>
          g.fill = Color.Black
          g.fillRect(i * cellWidth, j * cellHeight, cellWidth, cellHeight)
        case _ =>
      }
    }

    for (c <- level.characters) {
      c match {
        case p: Player =>
          g.drawImage(playerImg, p.x * cellWidth, p.y * cellHeight, cellWidth, cellHeight)
        case e: Enemy =>
          g.drawImage(enemyImg, e.x * cellWidth, e.y * cellHeight, cellWidth, cellHeight)
        case pr: Projectile =>
          g.drawImage(bulletImg, (pr.x * cellWidth) + cellWidth / 4, (pr.y * cellHeight) + cellHeight / 4, cellWidth / 4, cellHeight / 4)
        case _ =>
      }
    }
  }
}