package graphicgame

import scala.collection.mutable._

class Level(val style: Int, val elem: Array[Array[MapElements]], var characters: ListBuffer[Character]) {

  //mChars
  /*var items = mItems*/

  def element(x: Int, y: Int) = {
    elem(x)(y)
  }

  def addCharacter(c: Character): Unit = {
    characters += c
    c.setLevel(this)
  }

  def removeCharacter(c: Character): Unit = {
    characters -= c
    c.setLevel(null)
  }

  def elemOccupied(x: Int, y: Int): Boolean = {
    var check = false
    var index = 0
    while (index < characters.length && check == false) {
      if (characters(index).x == x && characters(index).y == y) {
        check = true
      }
      index += 1
    }
    check
  }

  def getElemChara(x: Int, y: Int): Character = {
    var chara: Character = null
    if (elemOccupied(x, y)) {
      var index = 0
      while (index < characters.length && chara == null) {
        if (characters(index).x == x && characters(index).y == y) {
          chara = characters(index)
        }
        index += 1
      }
    }
    chara
  }
}