package graphicgame

class Stairs(val destX: Int, val destY: Int) extends MapElements {
  var destLevel: Level = null
  def canPass(c: Character): Boolean = {
    false
  }

  def setDestLevel(level: Level) = {
    destLevel = level
  }
}