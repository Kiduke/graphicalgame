

package graphicgame

import scala.collection.mutable._

class Enemy(eX: Int, eY: Int) extends Character(eX, eY) {
  var pLevel: Level = null
  var moveCount = 1

  def setLevel(level: Level) = {
    pLevel = level
  }

  def update(): Unit = {

  }

  override def move(dX: Int, dY: Int): Unit = {
    if(pLevel.elemOccupied(dX, dY) && pLevel.getElemChara(dX, dY).isInstanceOf[Projectile]) {
      pLevel.getElemChara(dX, dY).asInstanceOf[Projectile].master.score += 1
      pLevel.getElemChara(dX, dY).asInstanceOf[Projectile].die()
      die()
    }
    else if (pLevel.elemOccupied(dX, dY) && pLevel.getElemChara(dX, dY).isInstanceOf[Player]) {
      pLevel.getElemChara(dX, dY).asInstanceOf[Player].die()
      x = dX
      y = dY
    } else if (pLevel.elemOccupied(dX, dY) && pLevel.getElemChara(dX, dY).isInstanceOf[Enemy]) {}
    else {
      x = dX
      y = dY
    }
  }

  def moveTowards(sx: Int, sy: Int, ex: Int, ey: Int): (Int, Int) = {
    val offsets = Array((1, 0), (-1, 0), (0, 1), (0, -1))
    val queue = collection.mutable.Queue[(Int, Int, List[(Int, Int)])]()
    val visited = collection.mutable.Set[(Int, Int)]()
    queue.enqueue((sx, sy, Nil))
    visited((sx, sy)) = true
    while (queue.nonEmpty) {
      val (x, y, path) = queue.dequeue()
      for ((ox, oy) <- offsets) {
        if (x + ox == ex && y + oy == ey && path.length > 1) {
          return path.tail.head
        }
        if (x + ox >= 0 && x + ox < pLevel.elem.length && y + oy >= 0 && y + oy < pLevel.elem(x).length && pLevel.elem(x + ox)(y + oy).canPass(this) && !visited((x + ox, y + oy))) {
          queue.enqueue((x + ox, y + oy, path :+ (x, y)))
          visited((x + ox, y + oy)) = true
        }
      }
    }
    null
  }

  def die() {
    dead = true
    pLevel.removeCharacter(this)
  }
}